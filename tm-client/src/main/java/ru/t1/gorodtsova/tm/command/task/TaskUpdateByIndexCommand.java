package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Update task by index";

    @NotNull
    private final String NAME = "task-update-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken(), index, name, description);
        getTaskEndpoint().updateTaskByIndex(request);
    }

}
